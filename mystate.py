'''
Created on Nov 2, 2012

@author: kosta
'''
import sys
class MyState(object):
    move_rules = {0: [1, 3], 1: [0, 2, 4], 2: [1, 5], 3: [0, 4, 6], 4: [1, 3, 5, 7], 5: [2, 4, 8], 6: [3, 7], 7: [4, 6, 8], 8: [5, 7]}
    def __init__(self, puzzle=[],parent=None,step_nr=0, **kwargs):
        self.puzzle = puzzle
        self.parent = parent
        self.step_nr = step_nr
        
    def get_successors(self):
        free_pos = self.puzzle.index(0)
        possible_moves = self.move_rules[free_pos]
        new_possible_states = []
        for move in possible_moves:
            new_possible_states.append(self.swap_tiles(move, free_pos))
        
        
        return new_possible_states
    
    
    def __str__(self):
        string = " ". join([str(self.puzzle[0]), str(self.puzzle[1]), str(self.puzzle[2])]) + "\n"
        string += " ".join([str(self.puzzle[3]), str(self.puzzle[4]), str(self.puzzle[5])])+ "\n"
        string += " ".join([str(self.puzzle[6]), str(self.puzzle[7]), str(self.puzzle[8])])+ "\n"
        return string
    
    def __eq__(self, other_obj):
        return self.puzzle == other_obj.puzzle
        
    def swap_tiles(self, x_index, y_index):
        new_state = MyState(parent=self, step_nr=self.step_nr+1)
        new_state.puzzle = list(self.puzzle)
        
        new_state.puzzle[x_index] = self.puzzle[y_index]
        new_state.puzzle[y_index] = self.puzzle[x_index]
        return new_state
    
    def in_states_list(self, list):
        for item in list:
            if str(item) == str(self):
                return True
        return False
    
    def in_states_list_of_tuples(self, list):
        for item in list:
            if str(item[1]) == str(self):
                return True
        return False
        
    
        