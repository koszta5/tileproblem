'''
Created on Nov 3, 2012

@author: kosta
'''
import random
import heapq
import time
from struct import pack

"""
I used cython to speed up python excecution (pypy) is a good thing too

To quickly check the visited states I used this:
http://stackoverflow.com/questions/1331187/converting-list-of-integers-into-a-binary-string-in-python

I also tried a an object version of the task, but i could not get it work properly thanks to python being VERY slow when working with objects

"""
class PriorityQueue():
    def __init__(self):
        self.queue = []
        
    def append(self, x):
        (puzzle, parent, heuristic_number) = x
        heapq.heappush(self.queue, (heuristic_number, x))
        
    
    def pop(self):
        try:
            heuristic_number,state = heapq.heappop(self.queue)
        except IndexError,e:
            print len(self.queue)
        return state

class SolvePuzzle():
    def __init__(self, init_puzzle=[],heuristic="man", **kwargs):
        if init_puzzle:
            self.init_puzzle = init_puzzle
        else:
            self.init_puzzle = [0,8,7,6,5,4,3,2,1]
            self.randomize()
            
        self.final_puzzle = [1,2,3,8,0,4,7,6,5]
        
        self.move_rules = {0: [1, 3], 1: [0, 2, 4], 2: [1, 5], 3: [0, 4, 6], 4: [1, 3, 5, 7], 5: [2, 4, 8], 6: [3, 7], 7: [4, 6, 8], 8: [5, 7]}
        if heuristic == "man":
            self.heuristic = self.manhattan
        if heuristic == "wrong":
            self.heuristic = self.wrongly_placed
        self.state_queue = PriorityQueue()
        self.visited_states = {}
            
    def manhattan(self, state):
        heuristic_for_state = 0
        for tile in state:
            heuristic_for_state += abs(state.index(tile) - self.final_puzzle.index(tile))
        return heuristic_for_state
    
    def wrongly_placed(self, state):
        heuristic_for_state = 0
        for tile in state:
            if state.index(tile) != self.final_puzzle.index(tile):
                heuristic_for_state += 1
        return heuristic_for_state
    
    def swap_tiles(self, puzzle, pos_x, pos_y):
        old_x = puzzle[pos_x]
        puzzle[pos_x] = puzzle[pos_y]
        puzzle[pos_y] = old_x
        return puzzle
    
    def print_solution(self):
        pass
    
    def get_succesors(self, puzzle):
        new_states = []
        free_pos = puzzle.index(0)
        for movable_tile in self.move_rules[free_pos]:
            new_states.append(self.swap_tiles(list(puzzle), free_pos, movable_tile))
        return new_states
    
        
    def randomize(self):
        random.shuffle(self.init_puzzle)
        print "Initial state is"
        print " ". join([str(self.init_puzzle[0]), str(self.init_puzzle[1]), str(self.init_puzzle[2])])
        print " ".join([str(self.init_puzzle[3]), str(self.init_puzzle[4]), str(self.init_puzzle[5])])
        print " ".join([str(self.init_puzzle[6]), str(self.init_puzzle[7]), str(self.init_puzzle[8])])
        
    def print_puzzle(self, puzzle):
        print "puzzle"
        print " ". join([str(puzzle[0]), str(puzzle[1]), str(puzzle[2])])
        print " ".join([str(puzzle[3]), str(puzzle[4]), str(puzzle[5])])
        print " ".join([str(puzzle[6]), str(puzzle[7]), str(puzzle[8])])
        
        
    def run(self):
        init_state = (self.init_puzzle, None, self.heuristic(self.init_puzzle))
        self.state_queue.append(init_state)
        while True:
            actual_state = self.state_queue.pop()
            (actual_puzzle, actual_parent, actual_heuristic) = actual_state
            packed = pack(9*'B', *actual_puzzle)
            self.visited_states[packed] =True
            if actual_puzzle == self.final_puzzle:
                
                break
            new_possible_puzzles = self.get_succesors(actual_puzzle)
            for new_puzzle in new_possible_puzzles:
                packed = pack(9*'B', *new_puzzle)
                if packed in self.visited_states:
                    continue
                else:
                    new_state = (new_puzzle, actual_state, self.heuristic(new_puzzle))
                    self.state_queue.append(new_state)
            if (len(self.visited_states) % 50 == 0):
                print "# of visited states: " +str(len(self.visited_states))
                
    
    
        
if __name__ == "__main__":
    solver = SolvePuzzle(heuristic="man", init_puzzle=[])
    start = time.time()
    solver.run()
    end = time.time()
    print end - start
            
        