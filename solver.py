'''
Created on Nov 2, 2012

@author: kosta
'''
from mystate import MyState
import random
import sys
import heapq
from struct import pack
from threading import Thread
class Queue():
    def __init__(self):
        self.queue = []
        
    def append(self, x):
        self.queue.append(x)
        
    def pop(self):
        out = self.queue[0]
        del(self.queue[0])
        return out

class PriorityQueue():
    def __init__(self):
        self.queue = []
        
    def append(self, x):
        heapq.heappush(self.queue, x)
        
    
    def pop(self):
        try:
            number,state = heapq.heappop(self.queue)
            return state
        except IndexError,e:
            print e
            print "This has no solution!!!"
            return None
            

class Stack():
    def __init__(self):
        self.queue = []
    
    def append(self, x):
        self.queue.append(x)
        
    def pop(self):
        out = self.queue[len(self.queue)-1]
        del(self.queue[len(self.queue)-1])
        return out

class Solver(object):
    
    def __init__(self, init_puzzle = [], heuristic="man", **kwargs):
        self.init_state = MyState()
        if init_puzzle:
            self.init_state.puzzle = init_puzzle
            
        else:
            self.init_state.puzzle = [0,8,7,6,5,4,3,2,1]
            self.randomize()
        if heuristic == "man":
            self.heuristic = self.manhattan
        else:
            if heuristic == "wrong":
                self.heuristic = self.wrongly_placed
        self.actual_state = self.init_state
        self.final_state = MyState(puzzle=[1,2,3,8,0,4,7,6,5])
        self.visited_states = {}
        self.state_queue = PriorityQueue()
        self.solved = False
        self.steps_to_final = 0
        
        
    def is_final(self, state):
        if state == self.final_state:
            return True
        else:
            return False
        
    def manhattan(self, possible_new_states):
        values = []
        for state in possible_new_states:
            heuristic_for_state = 0
            for tile in state.puzzle:
                heuristic_for_state += abs(state.puzzle.index(tile) - self.final_state.puzzle.index(tile))
                #heuristic_for_state += int(abs(n - 1 - i) / self.N) + (abs(n - 1 - i) % self.N)
            values.append((heuristic_for_state, state))
        return values
    
    def wrongly_placed(self, possible_new_states):
        values = []
        for state in possible_new_states:
            heuristic_for_state =0
            for tile in state.puzzle:
                if state.puzzle.index(tile) != self.final_state.puzzle.index(tile):
                    heuristic_for_state += 1
            values.append((heuristic_for_state, state))
        return values
        
    def randomize(self):
        random.shuffle(self.init_state.puzzle)
        print "Initial state is"
        print " ". join([str(self.init_state.puzzle[0]), str(self.init_state.puzzle[1]), str(self.init_state.puzzle[2])])
        print " ".join([str(self.init_state.puzzle[3]), str(self.init_state.puzzle[4]), str(self.init_state.puzzle[5])])
        print " ".join([str(self.init_state.puzzle[6]), str(self.init_state.puzzle[7]), str(self.init_state.puzzle[8])])
        
    
        
    def run(self):
        while True:
            packed_puzzle = pack(9*'B', *self.actual_state.puzzle)
            self.visited_states[packed_puzzle] = True
            if (len(self.visited_states) % 1000 ==0):
                print "# of visited states: "+ str(len(self.visited_states))
            if self.is_final(self.actual_state):
                print "final state is:"
                print self.actual_state
                self.solved = True
                self.steps_to_final = self.actual_state.step_nr
                break
            
            else:
                possible_new = self.actual_state.get_successors()
                states_eval = self.heuristic(possible_new)
                for state_tuple in states_eval:
                    new_state = state_tuple[1]
                    packed_puzzle = pack(9*'B', *new_state.puzzle)
                    if not packed_puzzle in self.visited_states:
                        self.state_queue.append(state_tuple)
                self.actual_state = self.state_queue.pop()
                if (len(self.visited_states) > 5000):
                    self.actual_state = None
                
                if not self.actual_state:
                    break
class Experiment(Thread):
    def __init__(self, puzzles,heur_type, **kwargs):
        super(Experiment,self).__init__(**kwargs)
        self.puzzles = puzzles
        self.heuristic = heur_type

    def run(self):
        self.solved = []
        self.steps = []
        self.results= []
        for i in range(0, len(self.puzzles)):
            s = Solver(heuristic=self.heuristic, init_puzzle=self.puzzles[i])
            s.run()
            self.solved.append(s.solved)
            self.steps.append(s.steps_to_final)
            if s.solved:
                self.results.append(s.actual_state)
            else:
                self.results.append(None)
            
        
def experiment(no_of_tests=20):
    exercise = []
    for i in range(0,no_of_tests):
        s = Solver()
        exercise.append(s.init_state.puzzle)
    
    wrong_thread = Experiment(puzzles=exercise, heur_type="wrong")
    man_thread= Experiment(puzzles=exercise, heur_type="man")
    
    wrong_thread.start()
    man_thread.start()
    
    wrong_thread.join()
    man_thread.join()
    
    no_of_man = 0
    no_of_wrong = 0
    
    for i in range(0, no_of_tests):
        if man_thread.results[i]:
            no_of_man += 1
        if wrong_thread.results[i]:
            no_of_wrong += 1
    avarage_step_for_man = sum(man_thread.steps) / float(no_of_man)
    avarage_step_for_wrong = sum(wrong_thread.steps) / float(no_of_wrong)
    print "============== EXPERIMENT =============="
    print "no_of_tests : "+str(no_of_tests)
    print "----------------------------------------"
    print " WRONG HEURISTIC RESULTS:               "
    print " no_of_solved: "+str(no_of_wrong)
    print " avarage_step_count: "+str(avarage_step_for_wrong)
    print "----------------------------------------"
    print " MANHATAN HEURISTIC RESULTS:               "
    print " no_of_solved: "+str(no_of_man)
    print " avarage_step_count: "+str(avarage_step_for_man)
    print "----------------------------------------"
    

    
        
        
            
        


if __name__ == "__main__":
    experiment(no_of_tests=20)
    sys.stdin.readline()
    
"""
============== EXPERIMENT ==============
no_of_tests : 100
----------------------------------------
 WRONG HEURISTIC RESULTS:               
 no_of_solved: 54
 avarage_step_count: 83.7407407407
----------------------------------------
 MANHATAN HEURISTIC RESULTS:               
 no_of_solved: 54
 avarage_step_count: 74.0740740741
----------------------------------------
"""    
                
                
                
"""
============== EXPERIMENT ==============
no_of_tests : 1000
----------------------------------------
 WRONG HEURISTIC RESULTS:               
 no_of_solved: 500
 avarage_step_count: 79.602
----------------------------------------
 MANHATAN HEURISTIC RESULTS:               
 no_of_solved: 500
 avarage_step_count: 75.738
----------------------------------------
"""
        
        
        